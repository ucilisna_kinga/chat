import {ThreadWithUsers} from './models/dto/ThreadWithUsers';
import {client, uuid} from './repository/db_connection';
import {ThreadEndPoint} from './end-points/ThreadEndPoint';
import {MessageEndPoint} from './end-points/MessageEndPoint';
import {UserEndPoint} from './end-points/UserEndPoint';
import {UserRestController} from './rest-end-points/UserRestController';
const express = require('express');

export const chat_app = express();
chat_app.allSockets = [];
const http = require('http').Server(chat_app);
export const io = require('socket.io')(http);


io.on('connection', (socket, username) => {
  console.log('Socket connection established');
  console.log('socket.handshake.query.username');
  console.log(socket.handshake.query.username);
  io.sockets.emit('newUserIsOnline', socket.handshake.query.username);

  const endPoints = {
    thread : new ThreadEndPoint(chat_app, socket),
    messages : new MessageEndPoint(chat_app, socket),
    users: new UserEndPoint(chat_app, socket)
  };

  // Bind events to endpoints handlers
  for (const endPoint in endPoints) {
    const handler = endPoints[endPoint].handler;
    for (const event in handler) {
      socket.on(event, handler[event]);
    }
  }

  chat_app.allSockets.push(socket);
  console.log('app allSockets length: ' + chat_app.allSockets.length);

  socket.on('disconnect', () => {
    io.sockets.emit('userOffline', socket.handshake.query.username);
    chat_app.allSockets.pop(socket);
    console.log('Socket connection Disconnected, num of sockets: ', chat_app.allSockets.length);
  });
});

UserRestController(chat_app);

let port = 3000;
if (process.argv[2] === 'stage') {
  port = 13000;
}

http.listen(port, function () {
  console.log('App started listening on port 3000');
});
