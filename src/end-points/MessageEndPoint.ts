import * as MessageService from '../service/MessageService';
import * as ThreadService from '../service/ThreadService';
import {client} from '../repository/db_connection';
import {messagesToMessagesDtoAndSort} from './../models/utilities/dtoConvertors/MessagesToMessagesDto';
import {io} from './../index';
import {Thread} from '../models/db/Thread';
import * as ThreadEndPoint from "./ThreadEndPoint";

export let MessageEndPoint = function (app, socket) {
  this.app = app;
  this.socket = socket;

  this.handler = {
    reqGetAllMessagesByThread: reqGetAllMessagesByThread.bind(this),    // and this.socket in events
    reqSaveMessageForThread: reqSaveMessageForThread.bind(this),    // and this.socket in events
    reqMakeThreadAndHisMessagesSeenAndUpdate: reqMakeThreadAndHisMessagesSeenAndUpdate.bind(this),    // and this.socket in events
    reqMakeThreadAndHisMessagesSeen: reqMakeThreadAndHisMessagesSeen.bind(this),    // and this.socket in events
    reqGetMessageCounter: reqGetMessageCounter.bind(this)    // and this.socket in events
  };
};

function reqGetAllMessagesByThread(data) {

  // const mockMessagesForThread1 = [
  //   { text: 'msg 1 from teacherMale', sendByUsername: 'teacherMale' },
  //   { text: 'msg 2 from teacherMale', sendByUsername: 'teacherMale' },
  //   { text: 'msg 3 from teacherFemale', sendByUsername: 'teacherFemale' },
  //   { text: 'msg 4 from teacherFemale', sendByUsername: 'teacherFemale' }];
  // const mockMessagesForThread2 = [
  //   { text: 'msg 1  from studentFemale --', sendByUsername: 'studentFemale' },
  //   { text: 'msg 2  from studentFemale --', sendByUsername: 'studentFemale' },
  //   { text: 'msg 3  from teacherFemale :)', sendByUsername: 'teacherFemale' },
  //   { text: 'msg 4  from teacherFemale ;D', sendByUsername: 'teacherFemale' }];
  //
  // if (thread_id === 'thread_id1') {
  //   this.socket.emit('resGetAllMessagesByThread', mockMessagesForThread1 );
  // } else if (thread_id === 'thread_id2') {
  //   this.socket.emit('resGetAllMessagesByThread', mockMessagesForThread2 );
  // } else {
  //   this.socket.emit('resGetAllMessagesByThread', ['message1', 'message2'] );
  // }
  MessageService.getAllMessagesByThreadId(client, data)
    .then( (messages) => {
      this.socket.emit('resGetAllMessagesByThread', messagesToMessagesDtoAndSort(messages) );
  });
}

function reqSaveMessageForThread(data) {
  MessageService.saveMessage(client, data).then( () => {
    MessageService.getAllMessagesByThreadId(client, data)
      .then( (messages) => {
        return MessageService.insertUnseenForThreadUsers(client, data.threadId, data.userId);
      })
      .then(() => {
        const tmpThread = new Thread(data.userId, data.threadId, '', data.messageText);
        return ThreadService.updateThreadTimeAndLastMsgAllThreadUsers(client, data.userId, tmpThread);
      })
      .then(() => {
        // emit to all sockets
        io.sockets.emit('newMessageInserted', {threadId: data.threadId} );
      });
  });
  // Change thread last time active - MAKE THREAD ACTIVE WHEN ADDING MESSAGE TO IT
}

function reqMakeThreadAndHisMessagesSeenAndUpdate(data) {
  MessageService.makeSeenAllThreadMessagesByUsername(client, data.userId, data.threadId)
    .then(() => {
      const threadWithUsersSorted =  ThreadService.getThreadsByUsername(client, data.userId)
        .then( threadsWithUsers => {
          const sortedThreadsWithUsers = threadsWithUsers.sort( function(a, b){
            return b.thread_last_time_active - a.thread_last_time_active;
          });
          // test
          return Promise.resolve(sortedThreadsWithUsers);
        });

      const finalReturnArray = Promise.all([
        MessageService.getThreadsWithNumberOfUnseenMsgs(client, data.userId),
        threadWithUsersSorted
      ]).then( dataPromiseAll => {
        const threadsWithUnseenMsgs = dataPromiseAll[0];
        for (const t_id in threadsWithUnseenMsgs) {
          dataPromiseAll[1].forEach( twusers => {
            if (twusers.thread_id === t_id) {
              twusers.numOfUnseenMessagesForActiveUser = threadsWithUnseenMsgs[t_id];
            }
          });
        }
        return Promise.resolve(dataPromiseAll[1]);
      });
      return finalReturnArray;
    })
    .then((returnData) => {
      this.socket.emit('resMakeThreadAndHisMessagesSeenAndUpdate', returnData );
    });
}


function reqMakeThreadAndHisMessagesSeen(data) {
  MessageService.makeSeenAllThreadMessagesByUsername(client, data.userId, data.threadId)
    .then(() => {
      this.socket.emit('resMakeThreadAndHisMessagesSeen', {} );
    });
}

function reqGetMessageCounter(data) {
  MessageService.getThreadsWithNumberOfUnseenMsgs(client, data.username).then( obj => {
    let counter = 0;
    for (const p in obj) {
      counter++;
    }
    this.socket.emit('resGetMessageCounter', counter);
  });
}
