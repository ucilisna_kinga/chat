import {Thread} from '../models/db/Thread';
import {User} from '../models/db/User';


export function saveThread(client, t: Thread, u: User) {

  const query = `Insert into threads_by_user
  ( username, thread_id, thread_last_time_active, last_message_text ) values(
  '${u.username}', '${t.thread_id}', '${t.thread_last_time_active}', '${t.last_message_text}' ); `;
  return client.execute(query);
}


export function getThreadsByUsername(client, username) {
  const query = `select * from threads_by_user where username = '${username}' `;
  return client.execute(query);
}

export function getThreadById(client, username: string, thread_id: string) {
  const query = `select * from threads_by_user where username = '${username}' AND thread_id = '${thread_id}'  `;
  return client.execute(query);
}

export function updateThreadTime(client, username, thread) {
  const newDate = (new Date()).getTime().toString();
   const query = `update threads_by_user SET thread_last_time_active = '${newDate}'
   where username = '${username}' AND thread_id = '${thread.thread_id}'  `;
  return client.execute(query);
}

export function updateThreadLastMsg(client, username, thread) {
  const newDate = (new Date()).getTime().toString();
  const query = `update threads_by_user SET last_message_text = '${thread.last_message_text}'
   where username = '${username}' AND thread_id = '${thread.thread_id}'  `;
  return client.execute(query);
}

