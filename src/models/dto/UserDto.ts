export class UserDto {

  private username: String;
  private firstname: String;
  private lastname: String;
  private avatarUrl: String;

  constructor(username: String, firstname: String, lastname: String, avatarUrl: String) {
    this.username = username;
    this.firstname = firstname;
    this.lastname = lastname;
    this.avatarUrl = avatarUrl;
  }
}
