export class User {

  private _username: String;
  private _firstname: String;
  private _lastname: String;
  private _avatarUrl: String;


  constructor(username: String, firstname: String, lastname: String, avatarUrl: String) {
    this._username = username;
    this._firstname = firstname;
    this._lastname = lastname;
    this._avatarUrl = avatarUrl;
  }

  get username(): String {
    return this._username;
  }

  set username(value: String) {
    this._username = value;
  }

  get firstname(): String {
    return this._firstname;
  }

  set firstname(value: String) {
    this._firstname = value;
  }

  get lastname(): String {
    return this._lastname;
  }

  set lastname(value: String) {
    this._lastname = value;
  }

  get avatarUrl(): String {
    return this._avatarUrl;
  }

  set avatarUrl(value: String) {
    this._avatarUrl = value;
  }
}
