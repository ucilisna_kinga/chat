import {Thread} from '../models/db/Thread';
import {User} from '../models/db/User';
import * as ThreadRepository from '../repository/ThreadRepository';
import * as UserRepository from '../repository/UserRepository';
import {ThreadWithUsers} from '../models/dto/ThreadWithUsers';
import {UserDto} from '../models/dto/UserDto';
import * as dtpConverter from './../models/utilities/dtoConvertors/UserToUserDto';


export function createThread(client, thread: Thread, users: Array<User> ) {
  /*
   * insert into threads_by_user  for each username
   *
   * username1 thread1  1111112222333  lasttextmessage
   * username2 thread1  1111112222333  lasttextmessage
   * username3 thread1  1111112222333  lasttextmessage
   * username4 thread1  1111112222333  lasttextmessage
   */

  const allThreadsPromises = [];
  users.forEach( u => {
    const p = ThreadRepository.saveThread(client, thread, u);
    allThreadsPromises.push(p);
  });

  const allUsersPromises = [];
  users.forEach( u => {
    const p = UserRepository.saveUser(client, thread, u);
    allUsersPromises.push(p);
  });

  return Promise.all(allThreadsPromises)
    .then( () => Promise.all(allUsersPromises))
    .then( () => { return Promise.resolve(thread); })
    .catch( () => { return  Promise.resolve(false); });

   // insert into users_by_thread

   // thread1  username1   firstname1 lastname1
   // thread1  username2   firstname2 lastname2
   // thread1  username3   firstname3 lastname3
   // thread1  username4   firstname4 lastname4
}

export function getThreadsByUsername(client, username: String) {
  const threadsByUsernameWithUsers = ThreadRepository.getThreadsByUsername(client, username)
    .then( dataThreads => {
      const usersByThreadsArray = [];
      dataThreads.rows.forEach( t => {
        usersByThreadsArray.push(UserRepository.getUsersByThread(client, t));
      });

      return Promise.all(usersByThreadsArray).then( data => {
        const threadsWithUsers: Array<ThreadWithUsers> = [];
        for (let i = 0; i < data.length; i++) {
          const t = dataThreads.rows[i];
          const threadUsers = data[i].rows;

          const arrUserDtos = dtpConverter.convertRawDbUsersToUsersDto(threadUsers);
          const trWithUsers = new ThreadWithUsers(t.username, t.thread_last_time_active, t.thread_id, arrUserDtos, t.last_message_text);

          threadsWithUsers.push(trWithUsers);
        }
        return Promise.resolve(threadsWithUsers);
      });
    });

  return threadsByUsernameWithUsers;
}

export function getThreadById(client, username, thread_id) {
  return ThreadRepository.getThreadById(client, username, thread_id);
}

export function updateThreadTimeAndLastMsgAllThreadUsers(client, username, thread) {
  return UserRepository.getUsersByThread(client, thread)
    .then( users => {
      const listUsersProm = [];
      users.rows.forEach( u => {
        listUsersProm.push(ThreadRepository.updateThreadLastMsg(client, u.username, thread));
      });
      return Promise.all(listUsersProm);
    })
    .then( () => {
      return ThreadRepository.updateThreadTime(client, username, thread);
    });
}

export function updateThread(thread: Thread) {

}


export function deleteUserFromThread() {

}

export function insertUserToThread() {

}


export function threadExistForSenderAndReceiver (client, sender: String, receiver: String) {
  const arr = [];

  const arrOfUsersByThreadsForSender = ThreadRepository.getThreadsByUsername(client, sender)
    .then((data) => {
      const senderThreads: Array<Thread> = data.rows;
      senderThreads.forEach( st => {
        arr.push(UserRepository.getUsersByThread(client, st));
      });
      return Promise.resolve(arr);
    }).then( (dataArr) => {

      return Promise.all(dataArr).then ((data) => {
        let thread_id = null;
        let threadFound = false;
        data.forEach( r => {
          if (r.rows.length === 2  ) {
            r.rows.forEach( (u) => {
              if (u.username === receiver) {
                thread_id = Promise.resolve(u.thread_id);
                threadFound = true;
              }
            });
          }
        });
        if (threadFound) {
          return thread_id;
        } else {
          return Promise.resolve('false');
        }
      });
    });

  return arrOfUsersByThreadsForSender;
}
