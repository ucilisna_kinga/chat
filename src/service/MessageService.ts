import {Message} from '../models/db/Message';
import * as MessageRepository from '../repository/MessageRepository';
import * as UserRepository from '../repository/UserRepository';
import * as db from './../repository/db_connection';
import {Thread} from "../models/db/Thread";


export function saveMessage(client, data) {
  // const data = {messageText: text, threadId: this.activeThread.thread_id, userId: this.activeUser.username};
  const message: Message = new Message(db.uuid(), data.threadId, data.messageText, ((new Date()).getTime()).toString() , data.userId );
  return MessageRepository.saveMessage(client, message);
}

export function getAllMessagesByThreadId(client, data) {
  // data = { threadId: this.activeThread.thread_id}
  // const data = {messageText: text, threadId: this.activeThread.thread_id, userId: this.activeUser.username};
  return MessageRepository.getAllMessagesByThreadId(client, data.threadId)
    .then( d => Promise.resolve(d.rows));
}



// unseen service methods
export function getThreadsWithNumberOfUnseenMsgs(client, username) {
  return MessageRepository.getAllUnseenMessageByUsername(client, username, 'false')
    .then( (data) => {
      let sortedArray = data.rows.sort( (a, b) => a.thread_id > b.thread_id ? 1 : a.thread_id < b.thread_id ? -1 : 0  );
      sortedArray = sortedArray.map( e => e.thread_id);
      const threadsWithNumOfUnseenMsgs = {};
      sortedArray.forEach(function(x) { threadsWithNumOfUnseenMsgs[x] = (threadsWithNumOfUnseenMsgs[x] || 0) + 1; });
      return Promise.resolve(threadsWithNumOfUnseenMsgs);
    });
}

export function insertUnseenForThreadUsers(client, threadId, userId) {

  // find all users by data.threadId
  const tmpThread = new Thread(userId, threadId, '', '');
  return UserRepository.getUsersByThread(client, tmpThread)
    .then((usersByThreadData) => {

      const listPromSaveUnseenMesgsThreadUsers = [];
      usersByThreadData.rows.forEach( u => {
        if (u.username !== userId) {
          listPromSaveUnseenMesgsThreadUsers
            .push(MessageRepository.saveUnseenMessage(client, threadId, u.username, 'false'));
        }
      });

      return Promise.all(listPromSaveUnseenMesgsThreadUsers);
    });
}

export function makeSeenAllThreadMessagesByUsername(client, username, thread_id) {
  return MessageRepository.getAllUnseenMessageByUsernameAndThreadId(client, username, thread_id, 'false')
    .then(data => {
      const idsArr = data.rows.map(e => e.id);
      return Promise.resolve(idsArr);
    })
    .then(ids => {
      const listUpdatePromUnseenMsgs = [];
      ids.forEach( id => {
        MessageRepository.updateUnseenMessageById(client, id, 'true');
      });
      return Promise.all(listUpdatePromUnseenMsgs);
    });
}
