import * as td from './../db/TablesDefinitions';
import * as db_test from './../config/db_test_connection';


export function freshTablesCreated() {
  return db_test.client.execute('drop table if exists threads_by_user')
    .then( () => db_test.client.execute('drop table if exists users_by_thread'))
    .then( () => db_test.client.execute('drop table if exists messages_by_thread'))
    .then( () => db_test.client.execute('drop table if exists unseen_messages'))
    .then( () => db_test.client.execute(td.threads_by_user) )
    .then( () => db_test.client.execute(td.users_by_thread) )
    .then( () => db_test.client.execute(td.messages_by_thread) )
    .then( () => db_test.client.execute(td.unseen_messages) );
}


export function tablesDeleted() {
  return Promise.all([
    db_test.client.execute('drop table if exists threads_by_user'),
    db_test.client.execute('drop table if exists users_by_thread'),
    db_test.client.execute('drop table if exists messages_by_thread'),
    db_test.client.execute('drop table if exists unseen_messages')
  ]);
}
