import * as ThreadEndPoints from './../../../src/end-points/ThreadEndPoint';
import * as SetupQueriesActions from '../../db/SetupQueriesActions';
import {User} from '../../../src/models/db/User';
import {Thread} from '../../../src/models/db/Thread';
import * as db_test from '../../config/db_test_connection';
import * as ThreadService from '../../../src/service/ThreadService';
import * as MessageRepository from '../../../src/repository/MessageRepository';


export function reqGetAllThreadsByUsernameTest() {
  const freshTablesCreated = SetupQueriesActions.freshTablesCreated();

  const threadsCreated = freshTablesCreated.then( () => {
    const user1: User = new User('teacherMale', 'Учител Име', 'Учител Презиме', 'teacher_male.jpg');
    const user2: User = new User('studentFemale', 'Ученичка Име', 'Ученичка Презиме', 'student_female.jpg');

    const user3: User = new User('teacherMale', 'Учител Име', 'Учител Презиме', 'teacher_male.jpg');
    const user4: User = new User('teacherFemale', 'Учителка Име', 'Учителка Презиме', 'teacher_female.jpg');

    const thread1: Thread = new Thread('thread_id_1', ((new Date()).getTime()).toString(), 'last message text');
    const thread2: Thread = new Thread('thread_id_2', ((new Date()).getTime()).toString(), 'last message text 2');

    return Promise.all([
      ThreadService.createThread(db_test.client, thread1, [user1, user2]),
      ThreadService.createThread(db_test.client, thread2, [user3, user4]),
      MessageRepository.saveUnseenMessage(db_test.client, 'thread_id_1', 'teacherMale', 'true'),
      MessageRepository.saveUnseenMessage(db_test.client, 'thread_id_2', 'teacherMale', 'true'),
      MessageRepository.saveUnseenMessage(db_test.client, 'thread_id_1', 'teacherMale', 'false'),
      MessageRepository.saveUnseenMessage(db_test.client, 'thread_id_1', 'teacherMale', 'false'),
      MessageRepository.saveUnseenMessage(db_test.client, 'thread_id_2', 'teacherMale', 'false'),
      MessageRepository.saveUnseenMessage(db_test.client, 'thread_id_2', 'teacherMale', 'false'),
      MessageRepository.saveUnseenMessage(db_test.client, 'thread_id_1', 'teacherMale', 'true'),
      MessageRepository.saveUnseenMessage(db_test.client, 'thread_id_2', 'teacherMale', 'false'),
      MessageRepository.saveUnseenMessage(db_test.client, 'thread_id_3', 'teacherMale', 'true'),
      MessageRepository.saveUnseenMessage(db_test.client, 'thread_id_3', 'teacherFemale', 'true'),
      MessageRepository.saveUnseenMessage(db_test.client, 'thread_id_3', 'teacherFemale', 'true'),
    ]);
  });

  threadsCreated.then( () => {
    return ThreadEndPoints.reqGetAllThreadsByUsername({username: 'teacherMale'});
  })
  .then( data => {
    console.log('data');
    console.log(data);
    SetupQueriesActions.tablesDeleted().then(() => console.log('tables  deleted '));
  });
}

reqGetAllThreadsByUsernameTest();
